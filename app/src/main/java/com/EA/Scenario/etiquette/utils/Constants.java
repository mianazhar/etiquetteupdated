package com.EA.Scenario.etiquette.utils;

/**
 * Created by Mian on 9/29/2015.
 */
public class Constants {
    public static final int TAKE_PICTURE_SIGN_UP = 1122;
    public static final int SELECT_PICTURE_SIGN_UP = 1123;
    public static final int TAKE_PICTURE_ADD_SCENARIO = 1124;
    public static final int SELECT_PICTURE_ADD_SCENARIO = 1125;

    public static final String SignUpFragmentTag = "SignUpFragment";
    public static final String AddScenarioFragmentTag = "AddScenarioFragment";
}
